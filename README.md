# Description

This project is a demonstration of some type of Wi-Fi [deauthentication
attack](https://en.wikipedia.org/wiki/Wi-Fi_deauthentication_attack).

I briefly describe the attack in [my PhD
thesis](http://www.theses.fr/2020REN1S002), section 1.1. The attack is also
described in the [docs of
aircrack-ng](https://www.aircrack-ng.org/doku.php?id=cracking_wpa).

In this attack, we assume that the attacker knows the pre-shared key of some
Wi-Fi access point.

In the current Wi-Fi protocol (802.11-2016), the session key of any connection
can be derived from the access point pre-shared key and from cryptographic
values sent in cleartext during the handshake.

The attacker thus intercepts the handshake at the beginning of a connection
between some device and the access point.

The attacker is then able to compute the session key, and hence to decrypt the
traffic of the connection.

If the connection is already up, the attacker can even send a deauthentication
message to the victim, spoofing the MAC address of the access point.

This will cause the victim to perform the handshake again, allowing the attacker
to decrypt subsequent messages.

# Requirements

In my implementation I use [scapy](https://scapy.net/) to send the
deauthentication message.

Note that the attack requires your Wi-Fi card to support monitor mode, which not
all Wi-Fi cards do.

Requirements:
-   scapy
-   tshark
-   iproute2
-   systemd

# Usage

## Getting Wi-Fi parameters

First set up the pre-shared key of target access point in wireshark.

Now connect to target access point.

Find the channel used by your connection using:

    iw dev

I assume that victim is using the same channel as you.

Copy the skeleton and make the script exectuable:

    cp wifiattack.skel wifiattack
    chmod +x wifiattack

Fill in your Wi-Fi interface name and the previously found channel number in
file `wifiattack`.

Put your interface into monitor mode using:

    ./wifiattack monitor

Scan nearby Wi-Fi frames using:

    ./wifiattack scan

From the scanned frames, deduce your access point's and victim's MAC addresses.

Fill their MAC addresses in file `wifiattack`.

## Performing the attack

Start a capture of the frames between the access point and the victim using:

    ./wifiattack capture

Open another terminal. Send a deauthentication message using:

    ./wifiattack deauth

You should see in your capture a new Wi-Fi handshake followed by decrypted
packets.

The capture is also saved to `captures/capture.pcap`, so that you can analyze it
later using wireshark display filters. For example, the analyze function shows
only DNS packets:

./wifiattack analyze
